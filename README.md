# binder-environment

Builds binder for use with e.g. https://mybinder.org that contains the common infrastructure neeed for online GROMACS tutorials

This follows the approach described in https://discourse.jupyter.org/t/how-to-reduce-mybinder-org-repository-startup-time/4956
